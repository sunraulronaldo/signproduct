module.exports = {
  // 选项...
  devServer: {
    host: '0.0.0.0',
    hot: true,
    proxy: {
      '/api': {
        target: 'http://192.168.1.100:8081/',//跨域要访问的地址及端口
        changeOrigin: true,
        secure: false,
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  },
}