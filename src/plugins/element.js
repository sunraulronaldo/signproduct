import Vue from 'vue'
import { 
  Button, 
  Container,
  Header,
  Aside,
  Main,
  Footer, 
  Image,
  Menu,
  Submenu,
  MenuItem,
  MenuItemGroup,
  Avatar,
  Carousel,
  CarouselItem,
  Tabs,
  TabPane,
  Tag,
  Row,
  Checkbox,
  CheckboxGroup,
  CheckboxButton,
  Radio,
  RadioButton,
  RadioGroup,
  Table,
  TableColumn,
  DatePicker,
} from 'element-ui'

Vue.use(Button);
Vue.use(Container);
Vue.use(Header);
Vue.use(Aside);
Vue.use(Main);
Vue.use(Footer);
Vue.use(Image);
Vue.use(Menu);
Vue.use(Submenu);
Vue.use(MenuItem);
Vue.use(MenuItemGroup);
Vue.use(Avatar);
Vue.use(Carousel);
Vue.use(CarouselItem);
Vue.use(Tabs);
Vue.use(TabPane);
Vue.use(Tag);
Vue.use(Row);
Vue.use(Checkbox);
Vue.use(CheckboxGroup);
Vue.use(CheckboxButton);
Vue.use(Radio);
Vue.use(RadioButton);
Vue.use(RadioGroup);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(DatePicker);

